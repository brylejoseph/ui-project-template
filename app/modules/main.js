require.config({
  paths: {
    'angular':'../../bower_components/angular/angular.min'
  },
  shim: {},
  packages: [],
  waitSeconds: 60
});
require([
  'angular',
  'home/home.module'
], function () {
  'use strict';

  angular.bootstrap(document,['homeModule']);

});
