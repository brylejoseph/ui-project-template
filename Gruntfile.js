module.exports = function (grunt) {

  var globalConfig = {
    images: 'app/images',
    css: 'app/styles',
    fonts: 'app/fonts',
    scripts: 'app/modules'
  };

  grunt.initConfig({
    globalConfig: globalConfig,
    connect: {
      options: {
        port: 9000,
        middleware: function (connect) {
          return [
            connect().use(
              '/bower_components',
              connect.static('./bower_components')
            ),
            connect().use(
              '/node_modules',
              connect.static('./node_modules')
            ),
            connect.static('./app')
          ];
        }
      },
      server: {}
    },
    watch: {
      scripts: {
        files: ['app/**/*.{js,html,css}']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('serve', ['connect', 'watch']);
};
